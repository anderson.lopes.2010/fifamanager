package com.lopes.fifamanager.model.exception;

import java.io.Serial;

public class EntityNotFoundException extends RuntimeException {
    
    @Serial
    private static final long serialVersionUID = -1243234319860987820L;

    public EntityNotFoundException(String message) {
        super(message);
    }
}
