package com.lopes.fifamanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Team implements Serializable {

    @Serial
    private static final long serialVersionUID = -4943832727370877113L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String nickName;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String stadium;

    @Column(nullable = false)
    private String logo;

    @Column(nullable = false)
    private String webPage;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String country;

    @Column(nullable = false, updatable = false)
    private String teamCode;

    public Team() {
    }

    public Team(Long id, String name, String nickName, String email, String stadium, String logo, String webPage, String city, String country, String teamCode) {
        this.id = id;
        this.name = name;
        this.nickName = nickName;
        this.email = email;
        this.stadium = stadium;
        this.logo = logo;
        this.webPage = webPage;
        this.city = city;
        this.country = country;
        this.teamCode = teamCode;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStadium() {
        return this.stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public String getLogo() {
        return this.logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getWebPage() {
        return this.webPage;
    }

    public void setWebPage(String webPage) {
        this.webPage = webPage;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTeamCode() {
        return this.teamCode;
    }

    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Team team = (Team) o;
        return this.id.equals(team.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + this.id +
                ", name='" + this.name + '\'' +
                ", nickName='" + this.nickName + '\'' +
                ", email='" + this.email + '\'' +
                ", stadium='" + this.stadium + '\'' +
                ", logo='" + this.logo + '\'' +
                ", webPage='" + this.webPage + '\'' +
                ", city='" + this.city + '\'' +
                ", country='" + this.country + '\'' +
                ", teamCode='" + this.teamCode + '\'' +
                '}';
    }
}
