package com.lopes.fifamanager.model.service;

import com.lopes.fifamanager.model.Team;
import com.lopes.fifamanager.model.exception.EntityNotFoundException;
import com.lopes.fifamanager.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TeamService {

    private final TeamRepository teamRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    public Team findById(Long id) {
        return this.teamRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Entity by id: " + id + " was not found."));
    }

    public Team addTeam(Team team) {
        team.setTeamCode(UUID.randomUUID().toString());
        return this.teamRepository.save(team);
    }

    public List<Team> findAllTeams() {
        return this.teamRepository.findAll();
    }

    public Team updateTeam(Team team) {
        return this.teamRepository.save(team);
    }

    public void deleteTeam(Long id) {
        this.teamRepository.deleteById(id);
    }

}
