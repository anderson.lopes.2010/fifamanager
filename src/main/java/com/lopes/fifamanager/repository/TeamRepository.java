package com.lopes.fifamanager.repository;

import com.lopes.fifamanager.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Long> {
}
